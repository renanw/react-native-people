/**
 * Retorna palavra com a primeira letra maiúscula
 * @param string
 * @returns {string}
 */

const capitalizeFirstLetter = string => {
    return string[0].toUpperCase() + string.slice(1);
};

export default capitalizeFirstLetter;
