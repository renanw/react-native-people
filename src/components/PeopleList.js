import React from 'react';
import {FlatList, StyleSheet} from 'react-native';

import PeopleListItem from './PeopleListItem';

const PeopleList = props => {
    const {peoples, onPressItem} = props;

    const items = peoples.map(people =>
        <PeopleListItem
            key={people.login.uuid}
            people={people}
            navigateToPeopleDetail={onPressItem}/>
    );

    return (
        <FlatList
            style={styles.container}
            data={peoples}
            renderItem={({item}) => (
                <PeopleListItem
                    people={item}
                    navigateToPeopleDetail={onPressItem}/>
            )}
            keyExtractor={(item) => item.login.uuid}
        />
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#e2f9ff'
    }
});

export default PeopleList;
